@echo off
cls

if not exist out mkdir out

cd src
javac -d ..\out xyz\ddmax\littleball\*.java
xcopy img ..\out\img\ /s/e/Y

cd ..\out
echo Main-Class: xyz.ddmax.littleball.Launch > manifest.txt
jar cfm LittleBall.jar manifest.txt -C img/ xyz/ .

java -jar LittleBall.jar