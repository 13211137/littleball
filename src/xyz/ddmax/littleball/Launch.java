package xyz.ddmax.littleball;

import xyz.ddmax.littleball.frames.MainFrame;

import javax.swing.*;
import java.io.IOException;

/**
 * Entry point of the game
 * Created by ddMax on 2016/3/10.
 */
public class Launch {

    public static void main(String[] args) throws IOException{
        MainFrame frame = new MainFrame();
        frame.pack();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
