package xyz.ddmax.littleball.base;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.IOException;

/**
 * Base component of the whole ball game
 * Created by ddMax on 2016/3/9.
 */
public class Component {
	// The x, y coordinator
	private int x = -1;
	private int y = -1;
	// Image of this component
	private Image image = null;
	// Speed of this component
	private int speed = 5;

	/**
	 * Constructor with image path
	 * @param path Image path
	 * @throws IOException
	 */
	public Component(String path) throws IOException {
		this.image = ImageIO.read(getClass().getResource(path));
	}

	/**
	 * Constructor with panel's width, height, path
	 * @param width width
	 * @param height height
	 * @param path path
	 * @throws IOException
	 */
	public Component(int width, int height, String path) throws IOException {
		this.image = ImageIO.read(getClass().getResource(path));
		this.x = width - image.getWidth(null) / 2;
	}

	/**
	 * Constructor with image's path, x, y coordinator
	 * @param x x
	 * @param y y
	 * @param path path
	 */
	public Component(String path, int x, int y) throws IOException {
		super();
		this.image = ImageIO.read(getClass().getResource(path));
		this.x = x;
		this.y = y;
	}

	/**==========================================**
	                Getters and setters
	/**==========================================**/
	public int getX() {
		return this.x;
	}


	public int getY() {
		return this.y;
	}

	public int getSpeed() {
		return this.speed;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public Image getImage() {
		return this.image;
	}
}
