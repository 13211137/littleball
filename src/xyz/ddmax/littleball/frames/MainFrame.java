package xyz.ddmax.littleball.frames;

import xyz.ddmax.littleball.services.Service;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;

/**
 * Main window of the game
 * Created by ddMax on 2016/3/9.
 */
public class MainFrame extends JFrame{
	// Width of JPanel
	private final int BALLPANEL_WIDTH = 615;
	// Height of JPanel
	private final int BALLPANEL_HEIGHT = 450;

	private BallPanel ballPanel = null;
	private Service service = null;
	// Define a timer
	private Timer timer = null;

	public MainFrame() throws IOException {
		super();
		initialize();
	}

	/**
	 * Initialize the main window frame
	 */
	public void initialize() throws IOException {
		this.setTitle("Little Ball");
		this.setResizable(false);
		this.setBackground(Color.BLACK);
		// Get the ball panel
		ballPanel = getBallPanel();
		// Instantiate the core service
		service = new Service(this, BALLPANEL_WIDTH, BALLPANEL_HEIGHT);

		// Setup refresh task
		setupRepaintTimer();

		this.add(ballPanel);

		// Setup key listener
		setupKeyListener();
	}

	/**
	 * We define a background task to repaint the ball panel
	 * every 0.05s (20 FPS/s) to make the game run
	 */
	private void setupRepaintTimer() {
		ActionListener task = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// Change and refresh ball position
				service.run();
				// Repaint the ball panel
				ballPanel.repaint();
			}
		};
		if (timer == null) {
			// Instantiate the timer
			timer = new Timer(50, task);
			timer.start();
		} else {
			timer.restart();
		}
	}

	/**
	 * Setup key press event listener
	 */
	private void setupKeyListener() {
		KeyListener[] listeners = this.getKeyListeners();
		if (listeners.length == 0) {
			KeyListener keyAdapter = new KeyAdapter() {
				public void keyPressed(KeyEvent ke) {
					service.setStickPos(ke);
				}
			};
			this.addKeyListener(keyAdapter);
		}
	}

	/**
	 * Singleton to instantiate ballPanel
	 */
	private BallPanel getBallPanel() {
		if (ballPanel == null) {
			ballPanel = new BallPanel();
			ballPanel.setPreferredSize(new Dimension(
					BALLPANEL_WIDTH, BALLPANEL_HEIGHT
			));
		}
		return ballPanel;
	}

	// The panel to draw all the components
	public class BallPanel extends JPanel {

		@Override
		public void paint(Graphics graphics) {
			// Call draw method in service to draw components
			service.draw(graphics);
		}
	}

}
