package xyz.ddmax.littleball.services;

import xyz.ddmax.littleball.base.Component;
import xyz.ddmax.littleball.frames.MainFrame;
import xyz.ddmax.littleball.sprites.Ball;
import xyz.ddmax.littleball.sprites.Brick;
import xyz.ddmax.littleball.sprites.Magic;
import xyz.ddmax.littleball.sprites.Stick;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.IOException;

/**
 * The core logic of the ball game
 * Created by ddMax on 2016/3/10.
 */
public class Service {
	// Frame arguments
	private int width;
	private int height;
	private MainFrame frame = null;
	// Components in the game
	private Stick stick = null;
	private Ball ball = null;
	private Component win = null; // Win image
	private Component gameOver = null; // Game over image
	private Brick[][] bricks = null;

	private Service() {}

	public Service(MainFrame frame, int width, int height) throws IOException {
		this.width = width;
		this.height = height;
		this.frame = frame;
		stick = new Stick(width / 2, height, "/img/stick.gif");
		// Set the initial ball position to over the stick
		ball = new Ball(width / 2, height, stick.getImage().getHeight(null),
			"/img/ball.gif");
		gameOver = new Component("/img/over.gif");
		win = new Component("/img/win.gif");
		bricks = initBricks("/img/brick.gif", 22, 6);
	}

	/**
	 * Initialize the bricks array
	 * @param path Image path of brick
	 * @param xSize Brick amount in horizontal
	 * @param ySize Brick amount in vertical
	 * @return Bricks array
	 * @throws IOException
	 */
	private Brick[][] initBricks(String path, int xSize, int ySize) throws IOException {
		Brick[][] bricks = new Brick[xSize][ySize];
		int x, y, random;
		int imageSize = 28;
		boolean isDisable;
		// Initialize the brick array
		for (int i = 0; i < xSize; i++) {
			for (int j = 0; j < ySize; j++) {
				random = (int) (Math.random() * 3);
				x = i * imageSize;
				y = j * imageSize;
				// If random is 0, no magic in the brick, if is 1, it has.
				isDisable = !(Math.random() < 0.8);
				if (isDisable) {
					random = 0;
				}
				Brick brick = new Brick(path, random, x, y);
				brick.setDisable(isDisable);

				brick.setX(x);
				brick.setY(y);
				bricks[i][j] = brick;
			}
		}
		return bricks;
	}

	/**
	 * Change ball and magic position
	 */
	public void run() {
		setBallPos();
		setMagicPos();
	}

	/**
	 * Set the position of the ball
	 */
	private void setBallPos() {
		// The speed is positive
		int absSpeedX = Math.abs(ball.getSpeedX());
		int absSpeedY = Math.abs(ball.getSpeedY());
		// The game is started but not ended
		if (ball.isStarted()) {
			// If ball is moving onto the left side
			if (ball.getX() - absSpeedX < 0) {
				// set X position
				ball.setX(ball.getImage().getWidth(null));
				// set opposite x direction
				ball.setSpeedX(-ball.getSpeedX());
			}
			// If ball is moving onto the right side
			if (ball.getX() + absSpeedX > width
					- ball.getImage().getWidth(null)) {
				ball.setX(width - ball.getImage().getWidth(null) * 2);
				ball.setSpeedX(-ball.getSpeedX());
			}
			// If ball is moving onto the top side
			if (ball.getY() - absSpeedY < 0) {
				// set Y position
				ball.setY(ball.getImage().getWidth(null));
				// set opposite y direction
				ball.setSpeedY(-ball.getSpeedY());
			}
			// If ball is moving onto the bottom side
			if (ball.getY() + absSpeedY > height
					- stick.getImage().getHeight(null)) {
				// Detect whether the ball collide the stick
				if (isHitStick(ball)) {
					// set Y position
					ball.setY(height - ball.getImage().getHeight(null) * 2);
					// set opposite y direction
					ball.setSpeedY(-ball.getSpeedY());
				}
			}
			// The case when the ball collide a brick
			for (int i = bricks.length - 1; i > -1; i--) {
				for (int j = bricks[i].length - 1; j > -1; j--) {
					// If hit
					if (isHitBrick(bricks[i][j])) {
						if (ball.getSpeedY() > 0) {
							// set opposite y direction
							ball.setSpeedY(-ball.getSpeedY());
						}
					}
				}
			}
			// Game over if ball's y more than panel height
			if (ball.getY() > height) {
				ball.setStop(true);
			}

			// Set new x position
			ball.setX(ball.getX() - (int) (Math.random() * 2)
					- ball.getSpeedX());
			// Set new y position
			ball.setY(ball.getY() - (int) (Math.random() * 2)
					- ball.getSpeedY());
		}
	}

	/**
	 * Check whether the brick is hit
	 * @param brick Brick
	 * @return true if brick is not disabled
	 */
	private boolean isHitBrick(Brick brick) {
		// Check if the brick is already disabled
		if (brick.isDisable()) {
			return false;
		}

		// X position of center of ball
		double ballX = ball.getX() + ball.getImage().getWidth(null) / 2;
		// Y position of center of ball
		double ballY = ball.getY() + ball.getImage().getHeight(null) / 2;
		// X position of center of brick
		double brickX = brick.getX() + brick.getImage().getWidth(null) / 2;
		// Y position of center of brick
		double brickY = brick.getY() + brick.getImage().getHeight(null) / 2;
		// Distance between ball center and brick center
		double distance = Math.sqrt(Math.pow(ballX - brickX, 2)
				+ Math.pow(ballY - brickY, 2));

		// If overlapped, return true
		if (distance < (ball.getImage().getWidth(null) + brick.getImage()
				.getWidth(null)) / 2) {
			// Disable the brick
			brick.setDisable(true);
			return true;
		}
		return false;
	}

	/**
	 * Check whether the ball hits the stick
	 * @param component Stick
	 * @return true if ball height + stick image height > stick height
	 */
	private boolean isHitStick(Component component) {
		// Get the component image
		Image image = component.getImage();
		// If stick was hit
		if (component.getX() + image.getWidth(null) > stick.getX()
				&& component.getX() < stick.getX() + stick.getPreWidth()
				&& component.getY() + image.getHeight(null) > stick.getY()) {
			return true;
		}
		return false;
	}

	/**
	 * Change the position of magic
	 */
	private void setMagicPos() {
		for (int i = 0; i < bricks.length; i++) {
			for (int j = 0; j < bricks[i].length; j++) {
				Magic magic = bricks[i][j].getMagic();
				if (magic != null) {
					// If the brick is disabled
					if (bricks[i][j].isDisable() && magic.getY() < height) {
						// Set the y position increasing
						magic.setY(magic.getY() + magic.getSpeed());
						// Change the stick length
						setStickWidth(magic);
					}
				}
			}
		}
	}

	/**
	 * Change the length of the stick
	 * @param magic Magic
	 */
	public void setStickWidth(Magic magic) {
		if (isHitStick(magic)) {
			magic.magicDo(stick);
		}
	}

	/**
	 * Set the position of stick
	 * @param ke KeyEvent
	 */
	public void setStickPos(KeyEvent ke) {
		// Set ball to start
		ball.setStarted(true);
		// Left key
		if (ke.getKeyCode() == KeyEvent.VK_LEFT) {
			if (stick.getX() - stick.SPEED > 0) {
				stick.setX(stick.getX() - stick.SPEED);
			}
		}
		// Right key
		if (ke.getKeyCode() == KeyEvent.VK_RIGHT) {
			if (stick.getX() + stick.SPEED < width - stick.getPreWidth()) {
				stick.setX(stick.getX() + stick.SPEED);
			}
		}
		// Space restart key
		if (ke.getKeyCode() == KeyEvent.VK_SPACE) {
			// Initialize the game
			try {
				frame.initialize();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Main draw process
	 * @param graphics Graphics
	 */
	public void draw(Graphics graphics) {
		// If win
		if (isWin()) {
			// Draw the win image
			graphics.drawImage(win.getImage(), win.getX(), win.getY(), width,
					height - 10, null);
		} else if (ball.isStop()) {
			// Draw the gameover image
			graphics.drawImage(gameOver.getImage(), gameOver.getX(), gameOver.getY(),
					width, height - 10, null);
		} else {
			// Clear old image
			graphics.clearRect(0, 0, width, height);
			// Draw stick
			graphics.drawImage(stick.getImage(), stick.getX(), stick.getY(), stick
					.getPreWidth(), stick.getImage().getHeight(null), null);
			// Draw ball
			graphics.drawImage(ball.getImage(), ball.getX(), ball.getY(), null);
			// Iterate to draw bricks
			for (int i = 0; i < bricks.length; i++) {
				for (int j = 0; j < bricks[i].length; j++) {
					Component magic = bricks[i][j].getMagic();
					// If the brick is not disabled
					if (!bricks[i][j].isDisable()) {
						// The gap between bricks is 1
						graphics.drawImage(bricks[i][j].getImage(), bricks[i][j]
								.getX(), bricks[i][j].getY(), bricks[i][j]
								.getImage().getWidth(null) - 1, bricks[i][j]
								.getImage().getHeight(null) - 1, null);
					} else if (magic != null && magic.getY() < height) {
						graphics.drawImage(magic.getImage(), magic.getX(), magic
								.getY(), null);
					}
				}
			}
		}
	}

	/**
	 * Whether the player wins
	 * @return boolean
	 */
	public boolean isWin() {
		// If all bricks is in disable state, then win.
		for (int i = 0; i < bricks.length; i++) {
			for (int j = 0; j < bricks[i].length; j++) {
				if (!bricks[i][j].isDisable()) {
					return false;
				}
			}
		}
		return true;
	}
}
