package xyz.ddmax.littleball.sprites;

import xyz.ddmax.littleball.base.Component;

import java.io.IOException;

/**
 * The ball object
 * Created by ddMax on 2016/3/9.
 */
public class Ball extends Component{
	// The x-axis speed
	private int speedX = 8;
	// The y-axis speed
	private int speedY = 10;
	// Is ball start moving
	private boolean started = false;
	// Is ball stop moving
	private boolean stop = false;

	/**
	 *
	 * @param width Panel width
	 * @param height Panel height
	 * @param offset offset of the ball
	 * @param path Image path
	 * @throws IOException
	 */
	public Ball(int width, int height, int offset, String path) throws IOException {
		super(width, height, path);
		this.setY(height - super.getImage().getHeight(null) - offset);
	}

	/**==========================================**
	                Getters and setters
	/**==========================================**/
	public void setSpeedX(int speed) {
		this.speedX = speed;
	}

	public void setSpeedY(int speed) {
		this.speedY = speed;
	}

	public void setStarted(boolean b) {
		this.started = b;
	}

	public void setStop(boolean b) {
		this.stop = b;
	}

	public int getSpeedX() {
		return this.speedX;
	}

	public int getSpeedY() {
		return this.speedY;
	}

	public boolean isStarted() {
		return this.started;
	}

	public boolean isStop() {
		return this.stop;
	}
}
