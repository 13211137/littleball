package xyz.ddmax.littleball.sprites;

import xyz.ddmax.littleball.base.Component;

import java.io.IOException;

/**
 * The brick object
 * When the brick was hit, it will generate LongMagic
 * or ShortMagic randomly
 *
 * Created by ddMax on 2016/3/9.
 */
public class Brick extends Component{
	// Define the magic
	private Magic magic = null;
	// Use to check the validation
	private boolean disable = false;

	public static final int MAGIC_LONG_TYPE = 1;
	public static final int MAGIC_SHORT_TYPE = 2;

	/**
	 *
	 * @param path Image path
	 * @param type Magic type, long or short
	 * @param x position x
	 * @param y position y
	 * @throws IOException
	 */
	public Brick(String path, int type, int x, int y) throws IOException {
		super(path);
		if (type == Brick.MAGIC_LONG_TYPE) {
			this.magic = new LongMagic("/img/long.gif", x, y);
		} else if (type == Brick.MAGIC_SHORT_TYPE) {
			this.magic = new ShortMagic("/img/short.gif", x, y);
		}
		if (this.magic != null) {
			this.magic.setX(x);
			this.magic.setY(y);
		}
	}

	/**==========================================**
	                Getters and setters
	/**==========================================**/
	public void setDisable(boolean disable) {
		this.disable = disable;
	}

	public boolean isDisable() {
		return this.disable;
	}

	public Magic getMagic() {
		return this.magic;
	}

	public void setMagic(Magic magic) {
		this.magic = magic;
	}
}
