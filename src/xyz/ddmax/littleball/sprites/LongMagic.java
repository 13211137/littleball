package xyz.ddmax.littleball.sprites;

import java.io.IOException;

/**
 * The long magic object
 * Stick will be stretched when collide with a long magic
 *
 * Created by ddMax on 2016/3/9.
 */
public class LongMagic extends Magic {

	public LongMagic(String path, int x, int y) throws IOException {
		super(path, x, y);
	}

	/**
	 * Do stretch the stick.
	 * @param stick Stick
	 */
	public void magicDo(Stick stick) {
		double imageWidth = stick.getImage().getWidth(null);
		// When stick is not stretched
		if (stick.getPreWidth() <= imageWidth) {
			// Double the length of stick
			stick.setPreWidth(stick.getPreWidth() * 2);
		}
	}
}
