package xyz.ddmax.littleball.sprites;

import xyz.ddmax.littleball.base.Component;

import java.io.IOException;

/**
 * Base abstract magic object.
 * A magic is the property dropped down randomly
 * when the ball hit a brick.
 *
 * Created by ddMax on 2016/3/9.
 */
public abstract class Magic extends Component {
	/**
	 * Empty constructor
	 * @param path File path
	 * @param x position x
	 * @param y position y
	 */
	public Magic(String path, int x, int y) throws IOException {
		super(path, x, y);
	}

	/**
	 * The functionality of the magic
	 * @param stick Stick
	 */
	public abstract void magicDo(Stick stick);
}
