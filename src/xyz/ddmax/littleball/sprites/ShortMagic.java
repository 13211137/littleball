package xyz.ddmax.littleball.sprites;

import java.io.IOException;

/**
 * The short magic object
 * Stick will be shrunk when collide with a long magic
 *
 * Created by ddMax on 2016/3/9.
 */
public class ShortMagic extends Magic {

	public ShortMagic(String path, int x, int y) throws IOException {
		super(path, x, y);
	}

	/**
	 * Do shrink the stick.
	 * @param stick Stick
	 */
	public void magicDo(Stick stick) {
		double imageWidth = stick.getImage().getWidth(null);
		// When stick is not shrunk
		if (stick.getPreWidth() >= imageWidth) {
			// Make half length of the stick
			stick.setPreWidth((int) (stick.getPreWidth() * 0.5));
		}
	}
}
