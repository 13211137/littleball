package xyz.ddmax.littleball.sprites;

import xyz.ddmax.littleball.base.Component;

import java.io.IOException;

/**
 * The stick object
 * Created by ddMax on 2016/3/9.
 */
public class Stick extends Component {
	// The constant speed
	public static final int SPEED = 20;
	// The initial width of the stick
	private int preWidth = 0;

	/**
	 * Constructor with its width, height and image path
	 * @param width Panel width
	 * @param height Panel weight
	 * @param path Image path
	 */
	public Stick(int width, int height, String path)
			throws IOException {
		super(width, height, path);
		this.setY(height - super.getImage().getHeight(null));
		// Set the initial width
		this.preWidth = super.getImage().getWidth(null);
	}

	/**==========================================**
            Getters and setters for preWidth
	/**==========================================**/
	public int getPreWidth() {
		return this.preWidth;
	}

	public void setPreWidth(int preWidth) {
		this.preWidth = preWidth;
	}
}
